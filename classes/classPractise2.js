class Student 
{
    constructor (studentID , firstName , lastName , phoneNumber , address , email) {
        this.sID = studentID;
        this.fName = firstName;
        this.lName = lastName;
        this.ph = phoneNumber;
        this.add = address;
        this.em = email;
    }

    get sID () {
        return this._sID;
    }
    set sID (value) {
        this._sID = value;
    }

    get fName () {
        return this._fName;
    }
    set fName (value) {
        this._fName = value;
    }

    get lName () {
        return this._lName;
    }
    set lName (value) {
        this._lName = value;
    }

    get ph () {
        return this._ph;
    }
    set ph (value) {
        this._ph = value;
    }

    get add () {
        return this._add;
    }
    set add (value) {
        this._add = value;
    }

    get em () {
        return this._em;
    }
    set em (value) {
        this._em = value;
    }

    StudentDetails () {
        return `STUDENT DETAILS\n--------------------\nStudent ID:\t${this.sID}\nFirst Name:\t${this.fName}\nLast Name:\t${this.lName}\nPhone Number:\t${this.ph}\nAddress:\t${this.add}\nEmail:\t${this.em}`;
    }
}

var student1 = new Student ("9930285" , "Troy" , "Prujean" , "0221998021" , "15 john st" , "9930285@student.toiohomai.ac.nz");

console.log (student1.StudentDetails());
