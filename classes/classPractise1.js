class Animal 
{
    // Default getters and setters for the class properties
    get animalName () {
        return this._animalName;
    }
    set animalName (value) {
        this._animalName = value;
    }

    get animalSound () {
        return this._animalSound;
    }
    set animalSound (value) {
        this._animalSound = value;
    }

    // Constructor for the animal class
    constructor (name , sound)
    {
        this.animalName = name;
        this.animalSound = sound;
    }

    // Method for string concatenation
    AnimalSays()
    {
        return `The animal ${this.animalName} makes the sound of ${this.animalSound}`;
    }

}

// Instantiate a blank object animal1 using the Animal class
var animal1 = new Animal ();

// Inputting property values
animal1.animalName = prompt ("Enter the name of an animal");
console.log (`Animal name is: ${animal1.animalName}`);

animal1.animalSound = prompt ("Enter the sound that the animal makes");
console.log (`Sound entered is: ${animal1.animalSound}`);

// Using getters to output the property values
console.log (`The ${animal1.animalName} says ${animal1.animalSound}`);


console.log (`${animal1.AnimalSays()}`);




