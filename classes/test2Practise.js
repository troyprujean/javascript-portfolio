/*----------------------------------------------------------------------\        
|                                                                       |
|   Client:         Smart Buy TVs Ltd                                   |
|   Title:          Test Practise                                       |
|   Version:        1.0 Beta                                            |
|   Purpose:        To capture smart TV information                     |
|   Environment:    Chrome browser                                      |
|   Author:         Troy Prujean                                        |
|   Date:           07/06/2018                                          |
|                                                                       |
\---------------------------Troy Prujean-------------------------------*/

// Definition of SmartTV class
class SmartTV {
    // Constructor for the SmartTV class properties
    constructor (make,model,size,cost) {
        this.TVMake = make; //referencing this.getter / setter which they define the class properties
        this.TVModel = model;
        this.ScreenSize = size;
        this.Price = cost;
    }

    // Default getter and setter for the tvMake property
    get TVMake () { return this.tvMake; } set TVMake (value) { this.tvMake = value; } // this.property definition

    // Default getter and setter for the tvModel property
    get TVModel () { return this.tvModel; } set TVModel (value) { this.tvModel = value; }

    // Default getter and setter for the screenSize property
    get ScreenSize () { return this.screenSize; } set ScreenSize (value) { this.screenSize = value; }

    // Default getter and setter for the price property
    get Price () { return this.price; } set Price (value) { this.price = value; }

    // Function to return the cost if paying with credit card - ccLevy is the parameter which will be defined in the main program as a user input
    CcPrice (ccLevy) {
        return this.price + ((ccLevy / 100) * this.price);
    }
}

// Instantiation of the new object from the Employee class
var tv1 = new SmartTV ();

// Passing the user inputs to the set methods
tv1.TVMake = prompt ("Welcome to Smart Buy Tvs Ltd.\n\nPlease enter the make of the Smart TV:");
tv1.TVModel = prompt ("Welcome to Smart Buy Tvs Ltd.\n\nPlease enter the model of the Smart TV:");
tv1.ScreenSize = prompt ("Welcome to Smart Buy Tvs Ltd.\n\nPlease enter the size of the screen (inches):");
tv1.Price = Number (prompt ("Welcome to Smart Buy Tvs Ltd.\n\nPlease enter the price of the Smart TV ($):"));

// Outputting to console using the get methods
console.log ("Smart Buy TVs Ltd. TV information is outlined below");
console.log ("---------------------------------------------------");
console.log (`TV Make: ${tv1.TVMake}`);
console.log (`TV Model: ${tv1.TVModel}`);
console.log (`Screen Size: ${tv1.ScreenSize}`);
console.log (`Price: $${tv1.Price}`);

// Retrieving user input for the credit card levy and assigning it to ccLevy
var ccLevy = prompt ("Please enter the credit card levy % applied to the transaction (if a credit card was used):");

// Outputting to console the credit card levy and price using the CcPrice function
console.log ("---------------------------------------------------");
console.log (`Credit Card Levy: %${ccLevy}`);
console.log (`Credit Card Price: $${tv1.CcPrice(ccLevy)}`);