class Calc
{
    constructor (num1 , num2 , num3)
    {
    this.n1 = num1;
    this.n2 = num2;
    this.n3 = num3;
    }

    get n1 () {
        return this._n1;
    }

    set n1 (value) {
        this._n1 = value;
    }

    get n2 () {
        return this._n2;
    }
    
    set n2 (value){
        this._n2 = value;
    }

    get n3 () {
        return this._n3;
    }

    set n3 (value) {
        this._n3 = value;
    }

    CalcAdd () {
        return this._n1 + this._n2 + this._n3;
    }

    CalcMult () {
        return (this._n1 * this._n2) * this._n3;
    }

    CalcAvg () {
        return (this._n1 + this._n2 + this._n3) / 3;
    }
}

var calculate = new Calc(10 , 20 , 30);

console.log ("Calculator");
console.log (`\nFirst number: ${calculate._n1}\nSecond number: ${calculate.n2}\nThird number: ${calculate.n3}`);
console.log (`\nThe numbers added are: ${calculate.CalcAdd()}`);
console.log (`\nThe numbers multiplied are: ${calculate.CalcMult()}`);
console.log (`\nThe average of the numbers is: ${calculate.CalcAvg()}`);