/*
Title: GST Calculator
Author: XXXXXX
Date: dd/mm/yyyy
Purpose: Given a dollar value (initialised variable), calculate and display the GST amount and the total GST inclusive amount
*/

//Declare and initialise variables and constants
 const GST = 15;                               //constant to hold GST rate (eg. 15%)
 var dollarAmount = 525;                       //variable to hold the given dollar ($) amount used in calculating GST       
 var GSTamount = 0;                            //variable to hold the calculated GST amount
 var totalInclusive = 0;                          //variable to hold the calculated total GST inclusive amount

//GST Calculations 
GSTamount =  (GST /  100) * dollarAmount;             //calculate the amount of GST on the given dollar value
totalInclusive = dollarAmount + GSTamount;            //calculate the total GST inclusive amount
            
//Display (log) the given data and calculated information (no dp formatting)
 console.log("the given dollar value is: $" + dollarAmount);                              //display given dollar value
 console.log("the GST amount for the given value will be: $" + GSTamount);                //display calculated GST amount
 console.log("the given amount after GST is: $ " + totalInclusive);                       //display calculated total GST inclusive amount

console.log("---------------------");                    //display a separator line
console.log("formatting to 2 decimal places the above results are:");
console.log("---------------------");

//Display/log given data and calculated information (formatted to 2 dp)
console.log("the given dollar value is: $" + dollarAmount.toFixed(2));                   //display given dollar value
console.log("the GST amount for the given value will be: $" + GSTamount.toFixed(2));    //display calculated GST amount
console.log("the given amount after GST is: $ " + totalInclusive.toFixed(2));           //display calculated total GST inclusive amount