console.log("hello world"); // basic message output command
var number1 = 10; // setting number1 text to the variable of number 10
var number2 = 20; 
    console.log("the first number is: " + number1); // outputting current variable result
    console.log("the second number is: " + number2);
var result = 0;
    console.log("the current result for the variable answer is: " + result);
result = number1 + number2; // calculating the result of adding the two variables number1 and number2 and setting the variable result to the calculated result
    console.log("the result of the two numbers combined is: " + result); // outputting the result of the calculation
    console.log(number1 + "+" + number2 + "=" + result); // string for the full equation

    // indentations help read the code easier and keeping different sections seperated

var string1 = "hello "; // setting string variables for text
var string2 = "Troy";
    console.log(string1 + string2); // string decantination
    var string2 = "world"; // variables can change at any point in time