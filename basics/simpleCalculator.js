/*
Title: Simple Calculator
Author: XXXXXX
Date: dd/mm/yyyy
Purpose: Perform +, -, *, / and % (modulus) operations on two initialised variables and display the result of the calculation.
*/

//Declare and initialise variables
var number1 = 100; //variable to hold a given first number used in calculations
var number2 = 30;  //variable to hold a given second number used in calculations       
var result = 0; //variable to hold the result of the calculations

//Perform addition and display result of addition
result = number1 + number2;
console.log(number1 + " + " + number2 + " = " + result);         //output to console
//document.writeln(number1 + " + " + number2 + " = " + result)    //output to web page
//document.writeln("<br>")                                        //"<br>", new line in web page
 

//Perform subtraction and display result of subtraction
result = number1 - number2;
console.log(number1 + " - " + number2 + " = " + result);

//Perform multiplication and display result of multiplication
result = number1 * number2;
console.log(number1 + " x " + number2 + " = " + result);

//Perform division and display result of division
result = number1 / number2;
console.log(number1 + " / " + number2 + " = " + result);

//Perform modulus (find remainder) and display remainder
var remainder = number1 % number2;
console.log("the remainder of the equation for " + number1 + " / " + number2 + " = " + remainder);
