let numberArray = new Array(5);

console.log ("Numbers displayed as entered:");
console.log ("-----------------------------");
for (i = 0 ; i < numberArray.length ; i ++)
{
    numberArray [i] = Number (prompt (`Enter 5 numbers.\nEnter number ${i+1} (array index ${i})`)); 
    console.log (`Number ${i+1} (Array Index ${i}: ${numberArray [i]})`);
}

console.log ("\n\nNumbers displayed in reverse:");
console.log ("-----------------------------");
for (i = 5 ; i > 0 ; i --)
{
    console.log (`Number ${i} (Array Index ${i-1}: ${numberArray [i-1]})`);
}