var numberArray = [];
var minNum = 0;
var maxNum = 0;
var string = "";
var total = 0;

console.log ("Numbers displayed as entered:");
console.log ("-----------------------------\n\n");

numberArray.length = 5;

for (let i = 0 ; i < numberArray.length ; i ++)
{
    numberArray [i] = Number (prompt (`Enter 5 numbers.\nEnter number ${i + 1} (array index ${i})`));
    console.log (`Number ${i + 1} (array index ${i}) = ${numberArray [i]}`);
    total += numberArray [i];
    string += numberArray [i] + ", ";
}

minNum = numberArray [0];
maxNum = numberArray [0];

for (let j = 0 ; j < numberArray.length ; j ++)
{
    if (numberArray [j] > maxNum)
    {
        maxNum = numberArray [j];
    }
    if (numberArray [j] < minNum)
    {
        minNum = numberArray [j];
    }
}

console.log (`\n\nA string of the numbers in the array is: ${string}`);
console.log (`The total of the numbers in the array is: ${total}`);
console.log (`\nThe minimum number in the array is: ${minNum}`);
console.log (`The maximum number in the array is: ${maxNum}`);

