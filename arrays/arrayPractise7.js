let firstArray = new Array(5);
let secondArray = new Array (5);
var string = "";

console.log ("Numbers displayed when input and entered into the first array:");
console.log ("--------------------------------------------------------------\n");

for (let i = 0 ; i < firstArray.length ; i ++)
{
    firstArray [i] = Number (prompt (`Enter 5 numbers into the first array.\nEnter number: ${i+1}    Array index: ${i}`));
    console.log (`Number ${i+1}, Array index ${i}: ${firstArray [i]}`);
}

for (let j = 0 ; j < secondArray.length ; j ++)
{
    secondArray [j] = firstArray [j];
}

for (i = 0 ; i < firstArray.length ; i ++)
{
    string += secondArray [i] + ", ";
}

console.log ("\nItems have been copied from the first array to the second array.");
console.log (`\nSecond array items:           ${string}`);
