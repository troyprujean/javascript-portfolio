var arrayCount = 0;
var total = 0;
var string = "";

arrayCount = Number (prompt ("How many numbers would you like to store in the array?"));

let numberArray = new Array(arrayCount);

for (i = 0 ; i < arrayCount ; i ++)
{
    numberArray [i] = Number (prompt (`Number ${i + 1} (Array Index ${i})`));
    console.log(`Number ${i +1} (Array Index ${i}): ${numberArray[i]}`)
}

for (x of numberArray)
{
    total += x;
    string += x + ", "
}

console.log (`Array items entered:              ${string}`);
console.log (`Total of array entries:           ${total}`);