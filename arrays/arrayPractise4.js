let translator = ["Hello", "Kia ora", "Kon'nichiwa", "Namaste", "Bonjour", "Talofa"];
var userInput = 0;

userInput = Number (prompt ("I can translate 'Hello' in 5 different languages:\n1. Maori\n2. Japanese\n3. Hindi\n4. French\n5. Samoan\n\nWhich language translation do you wish to see (1 - 5)?"));

alert(`Your selected translation: ${userInput}\nHello is translated as:     ${translator [userInput]}`);