/*  
    Name: Troy Prujean
    Date: 09/04/2018
      ID: 9930285
*/      

let numberArray = new Array(5);
var minNum = 0;
var maxNum = 0;

//document.getElementById("demo1").innerHTML = "Numbers displayed when input and entered into the first array:";
//document.getElementById("demo2").innerHTML = "--------------------------------------------------------------\n";

for (let i = 0 ; i < numberArray.length ; i ++)
{
    numberArray [i] = Number (prompt (`Enter 5 numbers\nEnter number ${i+1} (Array index ${i})`));
    console.log (`Number ${i+1}, Array index ${i}: ${numberArray [i]}`);
}

for (let i = 0 ; i < numberArray.length ; i ++)
{
    // At the first iteration of the loop it will set the value of the min and max variables to the first array index value
    if (i == 0)
    {
        maxNum = numberArray [i];
        minNum = numberArray [i];
    }
    else
    // On the next interations of the loop it will check to see if the array indexes are greater or less than the min and max variables
    // If they are greater or less than the variables it will set the variables to the values
    {
        if (numberArray [i] > maxNum)
        {
            maxNum = numberArray [i];
        }
        else if (numberArray [i] < minNum)
        {
            minNum = numberArray [i];
        }
    }
}



document.getElementById("demo4").innerHTML = `\n\nThe minimum number is: ${minNum}`;
document.getElementById("demo5").innerHTML = `The maximum number is ${maxNum}`;