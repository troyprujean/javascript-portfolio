var string1 = "world";

// Declaration function concatenating a string and showing the parameter passed in the brackets (not required by JS but good to do)
function concatenation(_string1)
{
    var output = "hello";
    output = output + " " + string1;
    return output;
}

// Calling the function
document.getElementById("output1").innerHTML = concatenation(string1);