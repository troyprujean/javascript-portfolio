var input;

input = Number (prompt ("Calculate the circumference and area of a circle.\nEnter a radius:"));

var circumference = (_input) => (2 * 3.1416) * _input;

var area = (_input) => 3.1416 * (_input * _input);

document.getElementById ("output1").innerHTML = `Circle radius: ${input.toFixed (2)}\nCircle circumference: ${circumference (input).toFixed (2)}\nCircle area: ${area (input).toFixed (2)}`;