
// Prompting the user for inputs
var num1 = Number (prompt ("Enter the first number"));
var method = prompt ("Enter the symbol for calculation e.g. + - / *");
var num2 = Number (prompt ("Enter the second number"));

// Declaration function to add the two inputted variables
function Addition ()
{
    var total;
    // Switch case to determine calculation method
    switch (method)
    {
        case "+":
        total = num1 + num2;
        break;

        case "-":
        total = num1 - num2;
        break;

        case "/":
        total = num1 / num2;
        break;

        case "*":
        total = num1 * num2;
        break;
    }    
    return total;
}

// Outputting the HTML
document.getElementById("output1").innerHTML = `the first number entered was: ${num1}`;
document.getElementById("output2").innerHTML = `the method of calculation entered was: ${method}`;
document.getElementById("output3").innerHTML = `the second number entered was: ${num2}`;
document.getElementById("output4").innerHTML = `the result of the numbers combined is: ${Addition()}`;