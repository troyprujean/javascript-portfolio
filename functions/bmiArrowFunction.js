var inputWeight;
var inputHeight;
var personBMI;

inputWeight = Number (prompt ("Body Mass Index (BMI) calculator.\nEnter the persons weight (kg):"));
inputHeight = Number (prompt ("Enter the persons height (cm):"));

var BMI = (_inputWeight , _inputHeight) => _inputWeight / (_inputHeight / 100 * _inputHeight / 100);

personBMI = Number(BMI (inputWeight , inputHeight));

function BMIcategory (_personBMI)
{
    if (_personBMI >= 30)
    {
        return "Obese"
    }
    else if (_personBMI >= 25)
    {
        return "Overweight"
    }
    else if (_personBMI >= 18.5)
    {
        return "Normal weight"
    }
    else if (_personBMI < 18.5)
    {
        return "Underweight"
    }
    else
    {
        return "Error in program, cannot calculate BMI category"
    }
}

document.getElementById ("output1").innerHTML = `BMI calculator. The persons weight entered was: ${inputWeight}kgs. 
The persons height entered was: ${inputHeight}cms. 
The resulting BMI is: ${BMI (inputWeight , inputHeight).toFixed (1)}. 
The resulting BMI category is: ${BMIcategory (personBMI)}`;