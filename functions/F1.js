// Declaration function - can be called anywhere in the code
function Greeting ()
{
    return "Hello world - function declaration";
}

// Expression function - can only be called after the expression
var expression = function ()
{
    return "Hello world - function expression";
}

// calling the functions as outputs to the console
//console.log (Greeting());
document.getElementById("output1").innerHTML = Greeting();
//console.log (expression());
document.getElementById("output2").innerHTML = expression();