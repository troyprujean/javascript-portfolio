var input;
var translateArray = ["Kia Ora" , "Kon'nichiwa" , "Namaste" , "Bonjour" , "Talofa"];

input = Number (prompt ("I can translate hello in 5 different languages.\n1. Maori\n2.Japanese\n3.Hindi\n4.French\n5.Samoan\n\nWhich language translation do you wish to see (1 - 5)"));

var translate = (_input) => translateArray [_input - 1];

document.getElementById ("output1").innerHTML = `The number entered was ${input}. Hello translates to: ${translate (input)}` 