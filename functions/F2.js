num1 = 10;
num2 = 15;

// Declaration function
function Multiplication()
{
    var total;
    total = num1 * num2;
    return total;
}

// Expression function showing the variables that have been passed through (not required in javascript)
var totalE = function (_num1, _num2)
{
    return _num1 * _num2;
}

document.getElementById("output1").innerHTML = Multiplication();

document.getElementById("output2").innerHTML = totalE(num1,num2);