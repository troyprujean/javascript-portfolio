// Object 1
var cars1 = { year: "2010" , make: "Honda" , model: "Accord" }

// Object 2
var cars2 = {
    year:   "2017",
    make:   "Holden",
    model:  "Commodore"
}

// Object 3
var cars3 = new Object ()
    cars3.year =    "2004";
    cars3.make =    "Toyota";
    cars3.model =   "Hilux";

// Object 4
var cars4 = {
    year:   "1969",
    make:   "Corvette",
    model:  "Stingray"
}
// Assigning initial output string    
var outputString1 = "--- Car 1 --- <br>";
var outputString2 = "--- Car 2 --- <br>";
var outputString3 = "--- Car 3 --- <br>";

// Calling the output function
document.getElementById("output1").innerHTML = output(cars1 , outputString1) + "<br><br>";
document.getElementById("output2").innerHTML = output(cars2 , outputString2) + "<br><br>";
document.getElementById("output3").innerHTML = output(cars3 , outputString3) + "<br><br>";

// Function to concatenate and output string of the values held in the objects
function output (_cars1 , _outputString1)
{   
    for (let x in _cars1)
    {
      _outputString1 += _cars1[x] + " ";
    } 

    return _outputString1
}
