// Task A
var string1 = "";
var fullName1 = "";
var fullName2 = "";

var person1 = {
    firstName:  "Hemi",
    lastName:   "Turei",
    age:        "19",
    study:      "Part Time"
}

var person2 = {
    firstName:  "Saanvi",
    lastName:   "Patel",
    age:        "22",
    study:      "Full Time"
}

document.getElementById("output1").innerHTML = taskA (person1 , string1) + "<br>"
document.getElementById("output2").innerHTML = taskA (person2 , string1) + "<br>"

// Task B
fullName1 = person1.firstName + " " + person1.lastName;
fullName2 = person2.firstName + " " + person2.lastName;

document.getElementById("output3").innerHTML = fullName1;
document.getElementById("output4").innerHTML = fullName2;

// Task C
person1.age = "21";
person2.age = "24";

document.getElementById("output5").innerHTML = taskA (person1 , string1) + "<br>"
document.getElementById("output6").innerHTML = taskA (person2 , string1) + "<br>"

// Task D
person1.nationality = "New Zealand";
person2.nationality = "India";

document.getElementById("output7").innerHTML = taskA (person1 , string1) + "<br>"
document.getElementById("output8").innerHTML = taskA (person2 , string1) + "<br>"

// Task E
delete person1.study
delete person2.study

document.getElementById("output9").innerHTML = taskA (person1 , string1) + "<br>"
document.getElementById("output10").innerHTML = taskA (person2 , string1) + "<br>"

// Function for task A (output)
function taskA (_person1 , _string1)
{
    
    for (let x in _person1)
    {   
        _string1 += _person1[x] + " ";      
    }
    return _string1;
}   