/*
Title:      Persons         (Intro To Objects - Methods)
            Uses ES5 style of function definitions for the object methods/functions.
            You can use the "this" keyword in these functions/methods.

            The "this" Keyword:
            In JavaScript, the thing called "this", is the object that "owns" the JavaScript code.
            The value of "this", when used in a function, is the object that "owns" the function.
            Note that "this" is not a variable. It is a keyword. You cannot change the value of "this".


Provide relevant comments here and throughout the program.

Run using “node” in a VS Code Terminal.
*/

//Define 2 object literals - person1 & person2 
//Each object has two methods/functions called "fullName" and "fullAllowance"
let person1 = {firstName: "Hemi", 
                lastName: "Turei", 
                age: 19, 
                study: "Part Time",

                //<<< COMPLETE CODE HERE - A                        //method (function) to provide the full name of person
                fullName: function()
                    {
                        return this.firstName + " " + this.lastName;    //method returns the full name
                    },                                                    
                
                fullAllowance:  function()                          //method (function) to determine if the person is eligible for full student allowance (Yes/No)
                    {
                        if (this.age >= 25) 
                            {
                                return "No"                //method returns "No" if age is >= 25
                            }
                            else 
                            {
                                return "Yes"
                            }                                //method returns "Yes" if age is < 25
                    }               
                }


let person2 = {firstName: "Saanvi", 
                lastName: "Patel", 
                age: 25, 
                study: "Full Time",

                //<<< COMPLETE CODE HERE - A                        //method (function) to provide the full name of person
                fullName: function()
                    {
                        return this.firstName + " " + this.lastName;    //method returns the full name
                    },                                                           
                
                fullAllowance:  function()                          //method (function) to determine if the person is eligible for full student allowance (Yes/No)
                    {
                        if (this.age >= 25) 
                            {
                                return "No"                //method returns "No" if age is >= 25
                            }
                            else 
                            {
                                return "Yes"
                            }                                //method returns "Yes" if age is < 25
                    }               
                }    
   

//Declare variables
let fullName1                                      //for person1's full name
let fullName2                                    //for person2's full name



//Display all the properties and property values of object "person1" and "person2"
//Will also display the object method (function) names
console.log("\nProperties of 'person1' and 'person2':")            
console.log(person1)
console.log(person2)

console.log("\n----------")



//Obtain full names for "person1" and "person2"
fullName1 = person1.fullName();      //<<< COMPLETE CODE HERE - B               //obtain person1's full name via the object's method
fullName2 = person2.fullName();      //<<< COMPLETE CODE HERE - B               //obtain person2's full name via the object's method


//Display the full name (first and last names) of "person1" and "person2"
console.log(`\n'person1' Full Name: ${fullName1}`)
console.log(`'person2' Full Name: ${fullName2}`)

console.log("\n----------")


//Display eligibility of "person1" and "person2" for full student loans
console.log(`\n'person1' Eligibility for Full Allowance: ${person1.fullAllowance()}`)                         //<<< COMPLETE CODE HERE - C
console.log(`'person2' Eligibility for Full Allowance: ${person2.fullAllowance()}`)                           //<<< COMPLETE CODE HERE - C

console.log("\n----------")


console.log("")