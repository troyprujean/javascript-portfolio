/*
Title:  System-2 UNFINISHED  (Intro to 2D Arrays and the use of nested "for" loops)
Date:   XX
Author: YY
Purpose:See how to use nested "for" loops to access and display array elements

        2 Dimensional arrays in JavaScript are essentially an array of arrays - forming rows and columns.

Read the comments in the code       
Run the program via node in a VS Code Terminal
*/


//A 2D array is an array of arrays.
//This array has 3 items, each item is itself an array of 3 items.
//Declaring your array as shown below, makes it more obvious that you essentially have 3 rows and 3 columns
//The columns are the computer system's Vendor, Type and Model
let system = [ 
                ["Lenovo", "Ulta Notebook",      "ThinkPad L470"],          //1st item in the array is an array of three items, ie. the 1st "row"
                ["Dell",   "Blade Server",       "PowerEdge M830"],         //2nd item in the array is an array of three items, ie. the 2nd "row"
                ["HP",     "All-in-One Desktop", "EliteOne 800 G3"],        //3rd item in the array is an array of three items, ie. the 3rd "row"
             ]


//Declare variables
//Set size of the array (to control the for loops)
let    rowNum = 3;              //number of rows
let    colNum = 3;              //number of columns




console.log("")

//1. Display array one item at a time in a nested for loop (as a list one item per line)
//To get a picture of how the nested loop operates, display the values of the loop control variables
console.log(`1. Display array one item at a time in a nested for loop`)
console.log(`--------------------------------------------------------`)
for (i = 0; i < rowNum; i ++)
{
    for (j = 0; j <colNum; j ++)
    {
        console.log (`${system [i] [j]} \t rownumber ${i} \t colnumber ${j}`); 
    }
}

//The outer for loop control variable "i" dictates what array row    we are processing
//The inner for loop control variable "j" dictates what array column we are processing




console.log()

//2. Display array (matrix) one row at a time in a nested for loop 
//It is required to build a string for each row to display
console.log(`2. Display array (matrix) one row at a time in a nested for loop`)
console.log(`-----------------------------------------------------------------`)
for (i = 0; i < rowNum; i++) 
{
    let rowStr = "";
    for (j =0; j < colNum; j ++)
    {
        rowStr += system [i] [j] + " ";
    }
    console.log (rowStr);
}

//The outer for loop control variable "i" dictates what matrix row    we are processing
//The inner for loop control variable "j" dictates what matrix column we are processing




console.log()