/*
Title:  System-1  UNFINISHED (Intro to 2D Arrays)
Date:   XX
Author: YY
Purpose:See how 2D arrays are declared and how they can be displayed.
        How to refer to array items - a "row" at a time and each individual element at a time.

        2 Dimensional arrays in JavaScript are essentially an array of arrays - forming rows and columns.

Read the comments in the code
Run the program via node in a VS Code Terminal
*/


//Declare the "system" array, makes it more obvious that you essentially have 3 rows and 3 columns
//The columns are the computer system's Vendor, Type and Model
let systemArr = [["Dell", "OptiPlex", "990"],
              ["HP", "Compaq Elite", "8300"],
              ["Acer", "Aspire", "ATC-217"]
             ]




console.log("")

//1. Display ALL the items in the array in "one go"
console.log('1. Display ALL the items in the array in "one go"')
console.log('-------------------------------------------------')
console.log (systemArr);


console.log("")

//2A. Display the array using a "for...of" loop
console.log('2A. Display the array using a "for...of" loop - displays by "row"')
console.log('-----------------------------------------------------------------')
for (x in systemArr)
{
    console.log (systemArr[x]);
}




console.log("")

//2B. Display the array using a "for" loop
console.log('2B. Display the array using a "for" loop - displays by "row"')
console.log('------------------------------------------------------------')
for (i = 0 ; i < systemArr.length ; i ++)
{
    console.log (systemArr[i]);
}




console.log("")

//3. Display an individual "row" in the array, eg. Dell row (vendor, type and Model)
console.log('3. Display an individual "row" in the array, eg. Dell (vendor, type and model)')
console.log('------------------------------------------------------------------------------')
console.log (systemArr [0]);




console.log("")

//4. Display an individual element in the array, eg. Dell (vendor)
console.log('4. Display an individual element in the array, eg. Dell (vendor)')
console.log('------------------------------------------------------------------')
console.log (systemArr [0] [0]);




console.log("")

//5. Display an individual element in the array, eg. Dell (type)
console.log('5. Display an individual element in the array, eg. Dell (type)')
console.log('-------------------------------------------------------------------')
console.log (systemArr [0] [1]);





console.log("")

//6. Display ALL the elements in the array - individually (displayed in rows)
console.log('6. Display ALL the elements in the array - individually (displayed in rows)')
console.log('---------------------------------------------------------------------------')
console.log (systemArr [0]);                                               //first   row - Lenovo  Ultra Notebook       ThinkPad L470
console.log (systemArr [1]);                                                  //second  row - Dell    Blade Server         PowerEdge M830
console.log (systemArr [2]);                                               //third   row - HP      All-in-One Desktop   EliteOne 800 G3



console.log("")