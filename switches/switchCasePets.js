let myFavouritePet = "parrots";
let userInput = "";

userInput = prompt("I keep the following types of pets - dogs, rabbits, chickens and parrots \n Guess which type of pet is my favourite");

switch(userInput)
{
    case "dogs":
        console.log("your guess is incorrect, my favourite pets are not dogs")
        break;

    case "rabbits":
        console.log("your guess is incorrect, my favourite pets are not rabbits")
        break;

    case "chickens":
        console.log("your guess is incorrect, my favourite pets are not chickens")
        break;

    case "parrots":
        console.log("your guess is correct, my favourite pets are parrots!")
        break;

    default:
        console.log(`unrecognized pet input ${userInput}`)
        break;
}

