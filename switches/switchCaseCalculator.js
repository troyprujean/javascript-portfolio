var str1 = "";
var str2 = "";
var result = 0;

str1 = prompt("Enter the first number");
let userInput1 = Number(str1); // converting the userinput from a string to a number (2 variables are required to convert input to number)

let userInputOperator = prompt("Enter the operator: +,-,* or /")

str2 = prompt("Enter the second number");
let userInput2 = Number(str2);

switch(userInputOperator)
{
    case "+":
    result = userInput1 + userInput2;
    console.log(`The result of ${userInput1} ${userInputOperator} ${userInput2} = ${result}`)
    break;

    case "-":
    result = userInput1 - userInput2;
    console.log(`The result of ${userInput1} ${userInputOperator} ${userInput2} = ${result}`)
    break;

    case "*":
    result = userInput1 * userInput2;
    console.log(`The result of ${userInput1} ${userInputOperator} ${userInput2} = ${result}`)
    break;

    case "/":
    result = userInput1 / userInput2;
    console.log(`The result of ${userInput1} ${userInputOperator} ${userInput2} = ${result}`)
    break;

    default:
    console.log(`${userInputOperator} is not a recognised operator in this program`)
}