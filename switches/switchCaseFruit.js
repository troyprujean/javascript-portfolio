let apples = 1.25;
let bananas = 3.15;
let kiwifruit = 4.65;
let oranges = 2.75;

let userInput = prompt("What variety of fruit do you want the price of? \n apples, bananas, kiwifruit or oranges?");

switch (userInput)
{
    case "apples":
    console.log(`apples cost $${apples} per kilo`)
    break;

    case "bananas":
    console.log(`bananas cost $${bananas} per kilo`)
    break;

    case "kiwifruit":
    console.log(`kiwifruit costs $${kiwifruit} per kilo`)
    break;

    case "oranges":
    console.log(`oranges cost $${oranges} per kilo`)
    break;

    default:
    console.log(`${userInput} is an unrecognised fruit variety`)


}