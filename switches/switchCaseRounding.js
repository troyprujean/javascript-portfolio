var userInput
var output

userInput = Number(prompt("Program to determine what rounding to apply to a cash transaction.\nEnter the last cent value of the transaction (1 - 9):"))

switch (userInput)
{
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    output = "Round down"
    break;

    case 6:
    case 7:
    case 8:
    case 9:
    output = "Round up"
    break;

    default:
    output = "Incorrect input"
    break;
}


console.log("Determining rounding type for a cash transaction")
console.log("------------------------------------------------\n\n")
console.log(`Cash transactions ends with:    ${userInput} cents`)
console.log(`Rounding to apply:              ${output}`)