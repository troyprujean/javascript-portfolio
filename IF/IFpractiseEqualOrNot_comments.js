let num1 = 0;    // assigning a variable for the first input
let num2 = 0;   // assigning a variable for the second input
let str1 = "";  // assigning a variable for the message requesting the first input (string)
let str2 = "";  // assigning a variable for the message requesting the second input (string)
let message = ""; // assigning a variable for the resut

str1 = prompt("Please enter the first number: "); // prompting the user to input the first number and assigning the input to str1 variable
str2 = prompt("Please enter the second number: "); // prompting the user to input the second number and assigning the input to str2 variable
num1 = Number(str1); // setting the variable num1 to the value of str1 and changing it to a number not a string (JS inputs are automatically assigned to strings)
num2 = Number(str2); // setting the variable num2 to the value of str2 and changing it to a number not a string (JS inputs are automatically assigned to strings)

if (num1 === num2)
{
    message = "These are equal"  // condition checking to see if the first input is the same number as the second input. If true it will assign this message to the variable message
}
else if (num1 > num2)
{
    message = str1 + " is larger" // condition checking to see if the first input is greater than the second input. If true it will assign this message to the variable message
}
else 
{
    message = str2 + " is larger" // if both of the above conditions are not true e.g. num1 is < num2  it will assign this message to the variable message
}

console.log(`First Number: ${num1} Second Number: ${num2} \n ${message}`); // outputting the results in a string including the num1 and num2 inputs and the message (answer) variable