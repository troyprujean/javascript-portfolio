var num1;
var num2;
var num3;
var output;

num1 = Number (prompt("Enter the first number"));
num2 = Number (prompt("Enter the second number"));
num3 = Number (prompt("Enter the third number"));

if (num1 > num2 && num1 > num3)
{
    if (num2 > num3)
    {
        output = `${num3}-----${num2}-----${num1}`
    }
    else
    {
        output = `${num2}-----${num3}-----${num1}`
    }
}
else if (num2 > num1 && num2 > num3)
{
    if (num1 > num3)
    {
        output = `${num3}-----${num1}-----${num2}`
    }
    else
    {
        output = `${num1}-----${num3}-----${num2}`
    }
}
else if (num3 > num2 && num3 > num1)
{
    if (num2 > num1)
    {
        output = `${num1}-----${num2}-----${num3}`
    }
    else
    {
        output = `${num2}-----${num1}-----${num3}`
    }
}
else
{
    output = "There was an error"
}

console.log(`The result of the inputs you entered arranged in an ascending order is ${output}`);
