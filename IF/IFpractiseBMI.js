/*   Name: Troy Prujean
     Date: 21/03/2018
     ID:   9930285
*/     

// Declaring variables
var weight;
var heightCm;
var heightM
var BMI;
var BMItype;

// Assigning variables to user input
weight = Number (prompt("Body Mass Index (BMI) calculator.\nPlease enter the person's weight (kg):"));
heightCm = Number (prompt("Body Mass Index (BMI) calculator.\nPlease enter the person's height (cm):"));

// Converting height from CM to M
heightM = heightCm / 100;

// Calculating the BMI
BMI = weight / (heightM * heightM);

// Condition checking to determine the BMI type
if (BMI >= 30)
{
    BMItype = "Obese"
}
else if (BMI >= 25 && BMI < 30)
{
    BMItype = "Overweight"
}
else if (BMI >= 18.5 && BMI < 25)
{
    BMItype = "Normal weight"
}
else
{
    BMItype = "Underweight"
}

// Outputting result to console
console.log("Body Mass Index Calculator");
console.log("--------------------------\n");
console.log(`Person's height: ${heightCm} cms`);
console.log(`Person's weight: ${weight} kgs`);
console.log("--------------------------");
console.log(`BMI category: ${BMItype}`);
