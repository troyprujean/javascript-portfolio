let userPin1 = 0;
let userPin2 = 0;
let str1 = "";
let str2 = "";
let output1 = "";

str1 = prompt("Please enter a 4 digit pin number");
userPin1 = Number(str1);

if (userPin1 >= 1000)
{
    str2 = prompt("Please re-enter your 4 digit pin number")
    userPin2 = Number(str2)

    if (userPin1 === userPin2)
    {
        output1 = "Your PIN has been set!"
    }
    else
    {
        output1 = "Error! Your PIN numbers did not match. Your PIN was not set"
    }
    console.log(output1)
}
else
{
    console.log("This PIN number is not within the right parameters")
}