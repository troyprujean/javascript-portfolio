/*
    Name: Troy Prujean
    Date: 15/03/2018
      ID: 9930285
*/

var userMark;
var userGrade;
var userGradeA = 0;
var userGradeAPercent = 0;
var userGradeB = 0;
var userGradeBPercent = 0;
var userGradeC = 0;
var userGradeCPercent = 0;
var userGradeD = 0;
var userGradeDPercent = 0;
var userGradeF = 0;
var userGradeFPercent = 0;
var totalGrades;
var repeat;

// Added a loop so that it will keep running until the user enters anything other than y at the end
do
{
userMark = Number(prompt("Please enter your marks between 0 and 100"))

if (userMark > 100)
    {
        userGrade = "Error, the marks you have entered is greater than 100"
        console.log(`Your final marks: ${userMark}\t Final grade:  ${userGrade}`)
    }
else if (userMark < 0)
    {
        userGrade = "Error, the marks you have entered is less than 0"
        console.log(`Your final marks: ${userMark}\t Final grade:  ${userGrade}`)
    }
else if (userMark >= 90)
    {
        userGrade = "A+"
        console.log(`Your final marks: ${userMark}\t Final grade:  ${userGrade}`)
        userGradeA ++
    }
else if (userMark >= 80)
    {
        userGrade = "A"
        console.log(`Your final marks: ${userMark}\t Final grade:  ${userGrade}`)
        userGradeA ++
    }
else if (userMark >= 70)
    {
        userGrade = "B+"
        console.log(`Your final marks: ${userMark}\t Final grade:  ${userGrade}`)
        userGradeB ++
    }
else if (userMark >= 60)
    {
        userGrade = "B"
        console.log(`Your final marks: ${userMark}\t Final grade:  ${userGrade}`)
        userGradeB ++
    }
else if (userMark >= 50)
    {
        userGrade = "C"
        console.log(`Your final marks: ${userMark}\t Final grade:  ${userGrade}`)
        userGradeC ++
    }
else if (userMark >= 40)
    {
        userGrade = "D"
        console.log(`Your final marks: ${userMark}\t Final grade:  ${userGrade}`)
        userGradeD ++
    }
else
    {
        userGrade = "F"
        console.log(`Your final marks: ${userMark}\t Final grade:  ${userGrade}`)
        userGradeF ++
    }

repeat = prompt("Do you want to enter another final mark to grade? Y/y for yes, N/n for no").toLowerCase();
}
while (repeat == "y")

console.log("\n\nUser ended the program\n\n");

totalGrades = userGradeA + userGradeB + userGradeC + userGradeD + userGradeF;
userGradeAPercent = (userGradeA / totalGrades) * 100;
userGradeBPercent = (userGradeB / totalGrades) * 100;
userGradeCPercent = (userGradeC / totalGrades) * 100;
userGradeDPercent = (userGradeD / totalGrades) * 100;
userGradeFPercent = (userGradeF / totalGrades) * 100;
console.log(`A grades: ${userGradeA}`);
console.log(`Percentage of students with A grades: ${userGradeAPercent.toFixed(1)}%`);
console.log(`\nB grades: ${userGradeB}`);
console.log(`Percentage of students with B grades: ${userGradeBPercent.toFixed(1)}%`);
console.log(`\nC grades: ${userGradeC}`);
console.log(`Percentage of students with C grades: ${userGradeCPercent.toFixed(1)}%`);
console.log(`\nD grades: ${userGradeD}`);
console.log(`Percentage of students with D grades: ${userGradeDPercent.toFixed(1)}%`);
console.log(`\nF grades: ${userGradeF}`);
console.log(`Percentage of students with F grades: ${userGradeFPercent.toFixed(1)}%`);