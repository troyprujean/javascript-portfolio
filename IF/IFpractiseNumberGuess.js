/* Name: Troy Prujean
   Date: 15/03/2018
     ID: 9930285
*/     

// Initializing variables
let myNum = Number(5);
let userNum = 0;

// Prompting the user to guess the number I have set myNum to and storing the guess in the userNum variable as a number not a string
userNum = Number(prompt("Try and guess the number I am thinking of, the number is somewhere between 1 and 10, Please enter your guess"));

// Loop to run until the correct number is guessed
do
{  
    // If statement to determine if the user input is higher than myNum
   if (userNum > myNum)
   {
       userNum = Number(prompt(`Your input: ${userNum} is higher than mine, try and guess again`))
   } 
   // Else statement will run if the user input is less than myNum 
   else
   {
       userNum = Number(prompt(`Your input ${userNum} is lower than mine, try and guess again`))
   }
}  
// Until the userInput matches myNum the program will keep on looping
while (userNum != myNum)

// Once userNum matches myNum it spits you out of the loop and displays this console message
console.log(`Congratulations you guessed the right number it was ${myNum}`);
