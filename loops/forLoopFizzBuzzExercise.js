/*  Name: Troy Prujean
    Date: 26/03/2018
      ID: 9930285
*/      

// Declaring a variable
var num1;

// Loop to get the ascending numbers 1 - 100
for (num1 = 1 ; num1 <= 100 ; num1 ++)
{
    // Checking to see if the current number is a multiple of 3 or 5 first
    if (num1 % 3 === 0 && num1 % 5 === 0)
    {
        console.log ("FizzBuzz")
    }
    // Checking to see if the current number is a multiple of 3
    else if (num1 % 3 === 0)
    {
        console.log ("Fizz")
    }
    // Checking to see if the current number is a multiple of 5
    else if (num1 % 5 === 0)
    {
        console.log ("Buzz")
    }
    // If the current number is not a multiple of 3 or 5 just output the number
    else 
    {
        console.log (num1)
    }
}