/*  Name: Troy Prujean
    Date: 22/03/2018
      ID: 9930285
*/      

// Assigning variables
var rainfall = 0;
var month = 0;
var total = 0;

// Outputting list title outside of the loop
console.log("Rainfall (mm) for the year");
console.log("--------------------------\n\n");

// loop to run the loop 12 times using the month variable as the controller
for (month = 1 ; month <= 12 ; month ++)
{   
    // Asking the use for the monthly rainfall input using the loop iteration number (month variable) as the month identifier
    rainfall = Number(prompt(`Determine the total rainfall for the year:\nEnter the rainfall (mm) for month ${month} :`))
    // Adding each result of the monthly input (loop iteration) to the variable total
    total += rainfall
    // Outputting the monthly rainfall result
    console.log(`Rainfall for month ${month} : ${rainfall}`)
};

// Outputting the total rainfall result
console.log(`\n\nTotal rainfall        : ${total.toFixed(1)}`);
