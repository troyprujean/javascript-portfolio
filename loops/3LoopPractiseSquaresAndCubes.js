var x;

var square = function(x) // function that sets the value of variable "square" to x*x
    {
    return x*x; // returning the result of x*x to the variable square
    };
    
var cube = function(x) // function that sets the value of variable "cube" to x*square function
    {
    return x * square(x); // returning the result of x*square function to the variable cube
    };


console.log("\nNumber          Square          Cube")
console.log("------          ------          ----")
console.log("\n>>> for loop <<<")

for (x=0;x<=10;x++)
{
    console.log(`${x}------------------${square(x)}----------------${cube(x)}`) // template string to output the results of the variable x and the square and cube functions
}

x = 0;
console.log("\nNumber          Square          Cube")
console.log("------          ------          ----")
console.log("\n>>> while loop <<<")

while (x!=11)
{
    console.log(`${x}------------------${square(x)}----------------${cube(x)}`)
    x ++
}

x = 0;
console.log("\nNumber          Square          Cube")
console.log("------          ------          ----")
console.log("\n>>> do while loop <<<")

do
{
    console.log(`${x}------------------${square(x)}----------------${cube(x)}`)
    x ++       
}
while (x!=11)