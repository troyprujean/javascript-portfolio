var character = "";
var repeatValue = 0;
var str = "";

character = prompt ("Enter the character / symbol to be repeated:");
repeatValue = Number (prompt("Enter the `count`, ie. the number of times to repeat the character:"));

console.log (`Repeat ${character} ${repeatValue} times\n\n`);

console.log (">>> for loop <<<");


for (j = 1 ; j <= repeatValue ; j ++)
{
str += character 
}

console.log (str);

console.log (">>> while loop <<<");

i = 0;
str = "";

while (i != repeatValue)
{
    str += character
    i ++
}

console.log (str);

console.log (">>> do while loop <<<");

i = 0;
str = "";

do
{
    str += character
    i ++
}
while (i != repeatValue)

console.log (str);