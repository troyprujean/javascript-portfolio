/* 
    Name: Troy Prujean
    Date: 15/03/2018
      ID: 9930285
*/      

var input = 0;
var total = 0;

// Do while loop that will keep looping until the user enters "0"
do
{   
    // Setting the variable input to the result of the prompt
    input = Number(prompt("Enter the cost of the item you are purchasing"))

    // If the input is still not "0", set the result of the variable total to itself + the input (this creates a running total of the inputs and assigns it to the total variable)
    if (input !== 0)
    {
        total = total + input
        console.log(`Purchase: ${input}`)
    }
    else
    {
        console.log("End of purchases")
    }
}
// Once the loop is broken e.g. "0" is entered in the prompt, it spits out the total
while (input !== 0)

console.log(`Total: ${total}`)