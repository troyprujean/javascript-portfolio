var userNum;
var num1;
var num2;

userNum = Number(prompt("Which times table do you want to see?"));

console.log(`${userNum} times table:\n---------------\n>>>for loop<<<\n`);

for (num1=1;num1<=12;num1++)
{
    num2 = userNum * num1
    console.log(`${num1} x ${userNum} = ${num2}`)
};

console.log(`\n\n${userNum} times table:\n---------------\n>>>while loop<<<\n`);

num1 = 1;

while (num1!=13)
{
    num2 = userNum * num1
    console.log(`${num1} x ${userNum} = ${num2}`)
    num1 ++  
};

console.log(`\n\n${userNum} times table:\n---------------\n>>>while loop<<<\n`);

num1 = 1;

do
{
    num2 = userNum * num1
    console.log(`${num1} x ${userNum} = ${num2}`)
    num1 ++ 
}
while(num1!=13);