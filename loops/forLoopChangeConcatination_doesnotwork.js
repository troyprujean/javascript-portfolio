var purchase = 2369;
var paid = 2500;
var change = paid - purchase;
var residual = 0;

var hundred = 0;
var fifty = 0;
var twenty = 0;
var ten = 0;
var five = 0;
var two = 0;
var one = 0;

console.log(`Your change is: ${change}`);

residual = change;

// Setting the variable of hundred to change / 100 and having no decimal places
hundred = Math.trunc(residual/100);
// Setting the variable residual (change) to the remainder of change divided by 100
residual = residual % 100;

fifty = Math.trunc(residual/50);
residual = residual % 50;

twenty = Math.trunc(twenty/20);
residual = residual % 20;

ten = Math.trunc(residual/10);
residual = residual % 10;

five = Math.trunc(residual/5);
residual = residual % 5;

two = Math.trunc(residual/2);
residual = residual % 2;

one = residual;

console.log("Your Change is made up of:");

console.log(`${hundred} x $100`);
console.log(`${fifty} x $50`);
console.log(`${twenty} x $20`);
console.log(`${ten} x $10`);
console.log(`${five} x $5`);
console.log(`${two} x $2`);
console.log(`${one} x $1`);

let changeString = "";

for (i = 1; i <= hundred; i++)
{           
    changeString = changeString + "100 ";    
}

for (i = 1; i <= fifty; i++)
{            
    changeString = changeString + "50 ";     
}

for (i = 1; i <= twenty; i++)
{            
    changeString = changeString + "20 ";     
}

for (i = 1; i <= ten; i++)
{            
    changeString = changeString + "10 ";     
}

for (i = 1; i <= five; i++)
{             
    changeString = changeString + "5 ";      
}

for (i = 1; i <= two; i++)
{             
    changeString = changeString + "2 ";      
}

for (i = 1; i <= one; i++)
{             
    changeString = changeString + "1 ";      
}

console.log(`Your change is $${change}: ${changeString}`);