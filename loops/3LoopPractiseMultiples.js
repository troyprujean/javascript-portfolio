var num1;
var num2;
var num3;
var num4;

console.log("N          N*10          N*100          N*1000");
console.log("-          ----          -----          ------");

console.log(">>> for loop <<<")

for (num1=3;num1<=7;num1++)
{
    num2 = num1 * 10
    num3 = num1 * 100
    num4 = num1 * 1000
    console.log(`${num1}            ${num2}            ${num3}            ${num4}`)
}

console.log(">>> while loop <<<")

num1 = 3

while (num1!=8)
{
    num2 = num1 * 10
    num3 = num1 * 100
    num4 = num1 * 1000
    console.log(`${num1}            ${num2}            ${num3}            ${num4}`)
    num1++
}

console.log(">>> do while loop <<<")

num1 = 3

do
{
    num2 = num1 * 10
    num3 = num1 * 100
    num4 = num1 * 1000
    console.log(`${num1}            ${num2}            ${num3}            ${num4}`)
    num1++
}
while (num1!=8)