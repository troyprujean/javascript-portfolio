/*
    Name: Troy Prujean
    Date: 19/03/2018
      ID: 9930285
*/

// Assigning variables
var myFavouritePet = "parrots";
var myNum = Number(6);
var userInputPet = "";
var userInputGame = "";
var userInputNumber = 0;

// Asking user what game they would like to play
do
{
    userInputGame = prompt("Would you like to play the number guess game or my favourite animal guess game\nTo play the number guess game type number\nTo play the my favourite animal guess fame type animal\nType exit to quit")

    // If the user picks animals it will run this code
    if (userInputGame === "animal")
    {
    
        // Loop to run the game until the correct guess is entered
        do
        {   
            // Asking the user to guess my favourite pet
            userInputPet = prompt("I keep the following types of pets - dogs, rabbits, chickens and parrots \n Guess which type of pet is my favourite")

            // Switch case to check their guess against my favourite pet variable
            switch(userInputPet)
            {
            case "dogs":
                console.log("your guess is incorrect, my favourite pets are not dogs")
                break;

            case "rabbits":
                console.log("your guess is incorrect, my favourite pets are not rabbits")
                break;

            case "chickens":
                console.log("your guess is incorrect, my favourite pets are not chickens")
                break;

            case "parrots":
                console.log("your guess is correct, my favourite pets are parrots!")
                break;

            default:
                console.log(`unrecognized pet input ${userInputPet}`)
                break;
            }
        }
        while (userInputPet != myFavouritePet)

    }
    // If the user picks number it will run this code
    else if (userInputGame === "number")
    {   
        // Asking the user to guess my number
        userInputNumber = Number(prompt("Try and guess the number I am thinking of, the number is somewhere between 1 and 10, Please enter your guess"))

        // Loop to run the game until the correct guess is entered
        do
        {   
            // If statement to check their guess against my number
            if (userInputNumber == myNum)
            {
                console.log(`Congratulations you guessed the right number it was ${myNum}`)
            }
            else if (userInputNumber > myNum)
            {    
                userInputNumber = Number(prompt(`Your input: ${userInputNumber} is higher than mine, try and guess again`))
            } 
            else
            {
                userInputNumber = Number(prompt(`Your input ${userInputNumber} is lower than mine, try and guess again`))
            }
        }  
        while (userInputNumber != myNum)

    ;
    }
    // If either animal or number is not entered in the initial input this code will run
    else
    {
    console.log("You have entered an incorrect game type")
    }
}
while (userInputGame == "animal" || "number" && userInputGame != "exit");
